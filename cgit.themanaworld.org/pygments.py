#!/usr/bin/env python3

# This script uses Pygments and Python3. You must have both installed
# for this to work.
#
# http://pygments.org/
# http://python.org/
#
# It may be used with the source-filter or repo.source-filter settings
# in cgitrc.
#
# The following environment variables can be used to retrieve the
# configuration of the repository for which this script is called:
# CGIT_REPO_URL        ( = repo.url       setting )
# CGIT_REPO_NAME       ( = repo.name      setting )
# CGIT_REPO_PATH       ( = repo.path      setting )
# CGIT_REPO_OWNER      ( = repo.owner     setting )
# CGIT_REPO_DEFBRANCH  ( = repo.defbranch setting )
# CGIT_REPO_SECTION    ( = section        setting )
# CGIT_REPO_CLONE_URL  ( = repo.clone-url setting )


import sys
import io
import pygments.lexers
from pygments import highlight
from pygments.lexers import guess_lexer
from pygments.lexers import guess_lexer_for_filename
from pygments.util import ClassNotFound
from pygments.formatters import HtmlFormatter


sys.stdin = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8', errors='replace')
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8', errors='replace')
data = sys.stdin.read()
filename = sys.argv[1]
formatter = HtmlFormatter(style='pastie', nobackground=True)

try:
	lexer = guess_lexer_for_filename(filename, data)
except ClassNotFound:
	# requires Python >= 3.10
	match filename:
		case "APKBUILD":
			lexer = pygments.lexers.shell.BashLexer()
		case _:
			lexer = guess_lexer(data)
except TypeError:
	lexer = TextLexer()

# highlight! :-)
# printout pygments' css definitions as well
sys.stdout.write('<style>')
sys.stdout.write(formatter.get_style_defs('.highlight'))
sys.stdout.write('</style>')
sys.stdout.write(highlight(data, lexer, formatter, outfile=None))
